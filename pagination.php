<?php 

$connect = mysqli_connect('localhost', 'root', '', 'ciblog');

// Pagination data
$record_per_page = 3;
$page = '';
$output = '';

if(isset($_POST['page'])) {
	$page = $_POST['page'];
}
else {
	$page = 1;
}

$start_form = ($page - 1)*$record_per_page;

$query = "SELECT * FROM posts LIMIT $start_form, $record_per_page";
$result = mysqli_query($connect, $query);
$output .= '
	<table class="table table-bordered">
		<tr>
			<th width="15%">Title</th>
			<th width="85%">Body</th>
		</tr>
';
while($row = mysqli_fetch_array($result)) {
	$output .= '
		<tr>
			<td>'.$row['title'].'</td>
			<td>'.$row['body'].'</td>
		</tr>
	';
}
$output .= '</table><br /><div align="center"';

// Pagination
$page_query = "SELECT * FROM posts";
$page_result = mysqli_query($connect, $page_query);
$total_records = mysqli_num_rows($page_result);
$total_pages = ceil($total_records/$record_per_page);

for($i = 1; $i <= $total_pages; $i++) {
	$output .= '<span class="pagination_link" style="cursor:pointer; padding:6px; border:1px solid #ccc;" id="'.$i.'">'.$i.'</span>';	
}

echo $output;